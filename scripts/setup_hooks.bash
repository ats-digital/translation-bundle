#!/usr/bin/env bash

PROJECT=$(git rev-parse --show-toplevel)
rm -rf ${PROJECT}/.git/hooks
ln -s ${PROJECT}/scripts/hooks ${PROJECT}/.git/hooks