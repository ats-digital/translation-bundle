<?php

namespace ATS\TranslationBundle\Tests\Manager;

use ATS\TranslationBundle\Cache\TranslationCacheWarmer;
use ATS\TranslationBundle\Document\TranslationEntry;
use ATS\TranslationBundle\Helper\TranslationEntryHelper;
use ATS\TranslationBundle\Manager\TranslationEntryManager;
use ATS\TranslationBundle\Service\TranslationEntryService;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Cache\Adapter\AbstractAdapter;

/**
 * TranslationEntryHelperTest
 */
class TranslationEntryHelperTest extends KernelTestCase
{

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();
        $this->teh = $kernel->getContainer()->get(TranslationEntryHelper::class);
    }

    public function testDotToArray()
    {
        $input = ['a.b.c' => 'foo', 'a.b.d' => 'bar'];

        $this->assertEquals($this->teh->convertDotToArray($input), [
            'a' => [
                'b' => [
                    'c' => 'foo', 'd' => 'bar']
                ]
        ]);
    }
}
