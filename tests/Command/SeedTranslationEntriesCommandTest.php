<?php

namespace ATS\TranslationBundle\Tests\Cache;

use ATS\TranslationBundle\Cache\TranslationCacheWarmer;
use ATS\TranslationBundle\Command\SeedTranslationEntriesCommand;
use ATS\TranslationBundle\Document\TranslationEntry;
use ATS\TranslationBundle\Manager\TranslationEntryManager;
use ATS\TranslationBundle\Service\TranslationEntryService;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * TranslationCacheWarmerTest
 */
class SeedTranslationEntriesCommandTest extends KernelTestCase
{

    /**
     * @var TranslationEntryService
     */

    private $translationEntryService;


    /**
     * @var DocumentManager
     */

    private $documentManager;

    /**
     * @var ManagerRegistry
     */

    private $documentRegistry;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->translationEntryService = $kernel->getContainer()->get("ats_translation.translation_entry_service");
        $this->documentRegistry = $kernel->getContainer()->get('doctrine_mongodb');
        $this->documentManager = $kernel->getContainer()->get('doctrine_mongodb')->getManager();
        $this->documentManager->getSchemaManager()->dropDatabases();
    }


    /**
     * Test WarmUp
     */
    public function testSeedTranslationEntries()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $application->add(
            (new SeedTranslationEntriesCommand())
        );

        $command = $application->find('ats:translation:entry:seed');
        $commandTester = new CommandTester($command);

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                '--file' => ['tests/Command/assets/fr.json', 'tests/Command/assets/en.json'],
                '--locale' => ['fr', 'en'],
                '--purge' => true,
            ],
            [
            'interactive' => false
            ]
        );
        $this->assertEquals($returnCode, 0);
        $this->assertEquals(count($this->translationEntryService->listTranslationEntries()), 2);
    }


    /**
     * Test Fail if locale and file mismatch
     */
    public function testFailIfLocaleAndFileMismatch()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $application->add(
            (new SeedTranslationEntriesCommand())
        );

        $command = $application->find('ats:translation:entry:seed');
        $commandTester = new CommandTester($command);

        $this->expectException(\InvalidArgumentException::class);
        $commandTester->execute(
            [
                'command' => $command->getName(),
                '--file' => ['tests/Command/assets/fr.json'],
                '--locale' => ['fr', 'en'],
            ],
            [
            'interactive' => false
            ]
        );
    }

    /**
     * Test Fail if wrong filepath
     */
    public function testFailIfWrongFilepath()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $application->add(
            (new SeedTranslationEntriesCommand())
        );

        $command = $application->find('ats:translation:entry:seed');
        $commandTester = new CommandTester($command);

        $this->expectException(\InvalidArgumentException::class);
        $commandTester->execute(
            [
                'command' => $command->getName(),
                '--file' => ['find/me/if/you/can'],
                '--locale' => ['fr'],
            ],
            [
            'interactive' => false
            ]
        );
    }
}
