<?php declare(strict_types=1);

namespace ATS\CoreBundle\Tests\Unit\DependencyIntection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use ATS\TranslationBundle\DependencyInjection\Configuration;
use PHPUnit\Framework\TestCase;

/**
 * ConfigurationTest
 *
 * @author Mohamed BEN ABDA <mbenabda@ats-digital.com>
 */
class ConfigurationTest extends TestCase
{
    /**
     * Test suits for getConfigTreeBuilder
     *
     * @covers ATS\TranslationBundle\DependencyInjection\Configuration::getConfigTreeBuilder
     */
    public function testGetConfigTreeBuilder()
    {
        $configuration = new Configuration();
        $this->assertInstanceOf(TreeBuilder::class, $configuration->getConfigTreeBuilder());
    }
}
