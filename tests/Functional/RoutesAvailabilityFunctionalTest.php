<?php

namespace ATS\TranslationBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RoutesAvailabilityFunctionalTest extends WebTestCase
{

    private $container;

    public function setUp()
    {
        $kernel = static::bootKernel();
        $this->container = $kernel->getContainer();
    }
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url, $method = "GET")
    {
        $client = self::createClient([], ['HTTP_HOST' => $this->container->getParameter('app_domain')]);
        $client->request($method, $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        return array(
            array('/translations/'),
            array('/api/translation/get-available-languages'),
            array('/api/translation/list'),
            array('/api/translation/id/get'),
            // ...
        );
    }
}
