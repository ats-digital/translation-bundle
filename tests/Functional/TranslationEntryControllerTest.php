<?php declare(strict_types=1);

namespace ATS\TranslationBundle\Tests\Functional;

use ATS\TranslationBundle\Cache\TranslationCacheWarmer;
use ATS\TranslationBundle\Document\TranslationEntry;
use ATS\TranslationBundle\Manager\TranslationEntryManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * TranslationEntryControllerTest
 *
 * @author Mohamed BEN ABDA <mbenabda@ats-digital.com>
 */
class TranslationEntryControllerTest extends WebTestCase
{
    /**
     * Test list_translation_entries
     */
    public function testListTranslationEntriesAction()
    {
        $client = static::createClient();

        $uri = '/api/translation/list';
        $client->request('GET', $uri);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Test get_compact_translations
     */
    public function testCompactTranslationsAction()
    {
        $client = static::createClient();

        $uri = '/api/translation/compact';
        $client->request('GET', $uri);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Test get_available_languages
     */
    public function testAvailableLanguagesAction()
    {
        $client = static::createClient();

        $uri = '/api/translation/get-available-languages';
        $client->request('GET', $uri);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Test add_new_language
     */
    public function testAddNewLanguageAction()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $dm = $container->get('doctrine_mongodb')->getManager();

        $translationEntry = new TranslationEntry('someKey', ['fr' => 'Une Valeur', 'en' => 'A Value']);
        $dm->persist($translationEntry);
        $dm->flush();

        $uri = '/api/translation/new-language';
        $client->request('POST', $uri, ['language' =>'it']);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Test init_keys
     */
    public function testInitKeysAction()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $uri = '/api/translation/keys/init';
        $keys = array('keys' => ['toBeDeleted', 'someKey']);

        $client->request('POST', $uri, [], [], [], json_encode($keys));

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Test translation_rest_get : getTranslationsByLanguage
     */
    public function testGetTranslationsByLanguageAction()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        //persist TE
        $dm = $container->get('doctrine_mongodb')->getManager();

        $translationEntry = new TranslationEntry('someKey', ['fr' => 'Une Valeur', 'en' => 'A Value']);
        $dm->persist($translationEntry);
        $dm->flush();

        // getLanguage
        $uriFr = '/api/translation/get/fr';
        $client->request('GET', $uriFr);
        $this->assertTrue($client->getResponse()->isSuccessful());

        $uriDe = '/api/translation/get/de';
        $client->request('GET', $uriDe);
        $this->assertFalse($client->getResponse()->isNotFound());
    }


    /**
     * Test new_translation_entry
     */
    public function testNewTranslationEntryAction()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $uri = '/api/translation/new';

        $translationEntry = ['someKey', ['fr' => 'Une Valeur', 'en' => 'A Value']];

        $client->request('POST', $uri, [], [], [], json_encode($translationEntry));

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Test update_translation_entry
     */
    public function testUpdateTranslationEntryAction()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $dm = $container->get('doctrine_mongodb')->getManager();

        $translationEntry = new TranslationEntry(
            'testUpdateTranslationEntryAction',
            ['fr' => 'Une Valeur', 'en' => 'A Value']
        );
        $dm->persist($translationEntry);
        $dm->flush();

        $uri = sprintf('/api/translation/%s/update', $translationEntry->getId());
        $newTranslationEntry = '{"key" : "someKey","values" : {"fr" : "Une aqqfsaaaa","en" : "A vvvv"}}';

        $client->request('PUT', $uri, [], [], [], $newTranslationEntry);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Test delete_translation_entry
     */
    public function testDeleteTranslationEntryAction()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $dm = $container->get('doctrine_mongodb')->getManager();
//        $dm->getSchemaManager()->dropDatabases();

        $toBeDeletedTranslationEntry = new  TranslationEntry(
            'toBeDeleted',
            ['fr' => 'à supprimer', 'en' => 'to be deleted']
        );
        $dm->persist($toBeDeletedTranslationEntry);
        $dm->flush();
//        $tem = $container->get('ats_translation.translation_entry_manager');
//        $appCache = $container->get('cache.app');
//
//        $cachWarmer = new TranslationCacheWarmer($tem, $appCache);
//        $cachWarmer->warmUp($container->getParameter('kernel.cache_dir'));

        $uri = sprintf('/api/translation/%s/delete', $toBeDeletedTranslationEntry->getId());

        $client->request('DELETE', $uri);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }
}