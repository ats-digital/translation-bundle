### General
---

This bundle supersedes the default Symfony approach for i18n manipulation. Instead of being code-centric (i.e. manipulating key/values in YAML files per translation domain), it adopts a database-centric approach.


### Features & Capabilities
---

- Database-centric i18n configuration (for Doctrine MongoDB ODM)
- Dedicated RESTful translation key/language manipulation
- Dedicated GUI for translation key/language manipulation

### Setup
---
1- Open a command console, enter your project directory and execute the following command to download the latest stable version of this bundle:

`composer require ats/translation-bundle`

2- Enable the bundle by adding the following line in the app/AppKernel.php:


```
// app/AppKernel.php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new ATS\TranslationBundle\ATSTranslationBundle(),
        ];

        // ...
    }
}

```

3- Run `bin/console assets:install --symlink` to initialize static assets for translation GUI.

### Configuration
---
This bundle requires one single mandatory configuration option to prepend an API prefix for i18n REST endpoints.
```
ats_translation:
    api:
        prefix: 'your_api_prefix' #e.g. 'api'
```

### Usage
---

##### REST API :

i18n manipulation is done via the following routes :

 `GET /{api_prefix}/translation/compact` :
 - lists all i18n key/values for *all* languages. Sample output :

```
{
	"en": {
		"foo": "bar"
	}
}
```

 `GET /{api_prefix}/translation/get-available-languages` :

 - lists all available languages. Sample output :

```
['en','fr','de']
```

 `POST /{api_prefix}/translation/keys/init` :
 - seeds i18n context from a set of key/values. This can prove useful when importing a set of translation keys from your frontend. Sample Payload :
 ```
 {
	"keys" : ["home.welcome","home.logout","home.profile"]
 }
 ```

 This will batch-insert the aforementioned i18n keys into the translation context.

##### GUI :

You can access the dedicated UI for i18n manipulation via {backend_host}/translations

### FAQ
---

1. How efficient is this approach performance-wise ?
	* Each i18n invalidation propagates the commited changes to Filesystem cache. All REST API operations rely on cache-first strategy.

2. How can I restrict access to i18n GUI/REST API ?
	* i18n API endpoints and GUI are public by default. You can safely rely on Symfony security components to restrict access. The following configuration sample would deny access to all non-admin users :

```
	security:
		# [ ... ]
	    access_control:
	        - { path: '/%api_prefix%/translation/*', role:  ROLE_ADMIN } # secures access to REST API
	        - { path: '/translations', role:  ROLE_ADMIN } # secures access to GUI
```

### Roadmap
---

* Doctrine ORM Support
* Parameter support in translation values
* General UI look-and-feel improvements

#### What's new?
[CHANGELOG.md](CHANGELOG)
