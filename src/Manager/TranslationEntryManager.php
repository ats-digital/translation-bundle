<?php declare (strict_types = 1);

namespace ATS\TranslationBundle\Manager;

use ATS\CoreBundle\Manager\AbstractManager;
use ATS\TranslationBundle\Document\TranslationEntry;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Psr\Cache\CacheItemPoolInterface;

class TranslationEntryManager extends AbstractManager
{
    /**
     *
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * @param ManagerRegistry $managerRegistry
     * @param CacheItemPoolInterface $cache
     * @param string $managerName
     */
    public function __construct(ManagerRegistry $managerRegistry, CacheItemPoolInterface $cache, $managerName = null)
    {
        $this->cache = $cache;

        parent::__construct($managerRegistry, TranslationEntry::class, $managerName);
    }

    /**
     * @param string $key
     * @param string $language
     * @return mixed
     */
    public function getByKeyAndLanguage($key, $language)
    {
        $cacheItem = $this->cache->getItem("$language.$key");

        if ($cacheItem->isHit() === true) {
            $translation = $cacheItem->get();
        } else {
            $translationEntry = $this->getOneBy(['key' => $key]);

            if ($translationEntry !== null) {
                $translation = $translationEntry->getValueForLanguage($language);
                $cacheItem->set($translation);
                $this->cache->save($cacheItem);
            } else {
                $translation = null;
            }
        }

        return $translation;
    }

    /**
     * @param string $language
     * @return mixed
     */
    public function getByLanguage($language)
    {
        $translations = [];

        $cacheItem = $this->cache->getItem('translation.keys');
        if ($cacheItem->isHit() === true) {
            $keys = $cacheItem->get();
        } else {
            $keys = $this->getDocumentRepository()->getAllKeys();
        }

        foreach ($keys as $key) {
            $translations[$key] = $this->getByKeyAndLanguage($key, $language);
        }

        return $translations;
    }

    public function getAvailableLanguages()
    {
        $allEntries = $this->getAll();
        $languages = [];

        foreach ($allEntries as $translationEntry) {
            foreach ($translationEntry->getValues() as $lang => $text) {
                $languages[] = $lang;
            }
        }

        return array_unique($languages);
    }

    /**
     * @param TranslationEntry $translation
     */
    public function update($translation)
    {
        $this->getDocumentRepository()
            ->save($translation);

        $key = $translation->getKey();

        foreach ($translation->getValues() as $language => $text) {
            $cacheItem = $this->cache->getItem("$language.$key");
            $cacheItem->set($text);
            $this->cache->save($cacheItem);
        }

        $translationKeysCacheItem = $this->cache->getItem('translation.keys');
        $keys = $translationKeysCacheItem->get();
        $keys[] = $key;
        $translationKeysCacheItem->set($keys);
        $this->cache->save($translationKeysCacheItem);
    }

    /**
     * @param string $id
     */
    public function deleteById($id)
    {
        $translation = $this->getOneBy(['id' => $id]);
        if ($translation !== null) {
            $translationKey = $translation->getKey();

            $cacheKeys = [];

            foreach ($translation->getValues() as $lang => $text) {
                $cacheKeys[] = "$lang.$translationKey";
            }

            foreach ($cacheKeys as $cacheKey) {
                $this->cache->deleteItem($cacheKey);
            }

            $translationKeysCacheItem = $this->cache->getItem('translation.keys');
            $keys = $translationKeysCacheItem->get();
            $keys = array_diff($keys, [$translationKey]);

            if (count($keys) !== 0) {
                $translationKeysCacheItem->set($keys);
                $this->cache->save($translationKeysCacheItem);
            } else {
                $this->cache->deleteItem('translation.keys');
            }

            $this->delete($translation);
        }
    }

    /**
     * @param array $keys
     * @param string $locale
     */
    public function initKeys($keys, $locale)
    {
        $tEntries = [];

        $this->deleteAll();

        foreach ($keys as $key) {
            $tEntries[] = new TranslationEntry($key, [$locale => null]);
        }

        $this->batchUpdate($tEntries);
    }
}
