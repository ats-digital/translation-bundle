<?php declare(strict_types=1);

namespace ATS\TranslationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ats_translation');
        $rootNode
            ->children()
                ->arrayNode('api')
                    ->children()
                        ->scalarNode('prefix')->end()
                    ->end()
                ->end() // api
            ->end();

        return $treeBuilder;
    }
}
