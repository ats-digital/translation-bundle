<?php

namespace ATS\TranslationBundle\Helper;

class TranslationEntryHelper
{

    /**
     * @see https://gist.github.com/wgrafael/8a9bb1a963042bc88dac
     * Convert array with key in dot notation for multidimensional array
     *
     * Example:
     * Input: [ "name.firstname" => "Rafael", "name.lastname" => "Dantas", "a.b.c" => "d" ]
     * Output: [ "name" => [ "firstname" => "Rafael", "lastname" => "Dantas" ], "a" => [ "b" => [ "c" => "d" ] ]
     *
     * @param array $array with key in dot notation
     * @return array
     * @author Rafael Dantas
     **/

    public function convertDotToArray(array $array) : array
    {
        $newArray = array();
        foreach ($array as $key => $value) {
            $dots = explode(".", $key);
            if (count($dots) > 1) {
                $last = &$newArray[ $dots[0] ];
                foreach ($dots as $k => $dot) {
                    if ($k === 0) {
                        continue;
                    }
                    $last = &$last[$dot];
                }
                $last = $value;
            } else {
                $newArray[$key] = $value;
            }
        }
        return $newArray;
    }
}
