<?php

namespace ATS\TranslationBundle\Command;

use ATS\TranslationBundle\Document\TranslationEntry;
use ATS\TranslationBundle\Service\TranslationEntryService;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SeedTranslationEntriesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ats:translation:entry:seed')
            ->setDescription('Seeds translation entries from input JSON file')
            ->addOption('file', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'f')
            ->addOption('locale', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'l')
            ->addOption('purge', null, InputOption::VALUE_NONE, 'p');
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $translationEntryService = $this->getContainer()->get(TranslationEntryService::class);

        /**
         * @var array
         **/

        $filePaths = $input->getOption('file');

        /**
         * @var array
         **/

        $locales = $input->getOption('locale');
        $shouldPurge = $input->getOption('purge');

        if (count($locales) !== count($filePaths)) {
            throw new InvalidArgumentException("file and locale count mismatch");
        }

        if ($shouldPurge === true) {
            $translationEntryService->deleteAllTranslationEntries();
        }

        $rawEntries = [];
        $entries = [];

        foreach ($filePaths as $index => $filePath) {

            /**
             * @var string|false
             **/

            $i18nFile = @file_get_contents($filePath);

            if ($i18nFile === false) {
                throw new \InvalidArgumentException("Failed to read or find $filePath");
            }

            $i18nRawEntries = json_decode($i18nFile, true);

            $flattenedEntries = $this->flatten($i18nRawEntries);

            foreach ($flattenedEntries as $key => $value) {
                $locale = $locales[$index];
                $rawEntries[$key][$locale] = $value;
            }
        }

        foreach ($rawEntries as $key => $values) {
            $entry = new TranslationEntry();
            $entry->setValues($values)->setKey((string) $key);
            $entries[] = $entry;
        }

        $translationEntryService->seedTranslationEntries($entries);
    }

    protected function flatten(array $array, string $prefix = '')
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value) === true) {
                $result = $result + $this->flatten($value, $prefix . $key . '.');
            } else {
                $result[$prefix . $key] = $value;
            }
        }
        return $result;
    }
}
