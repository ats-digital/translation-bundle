### Coding Style
* coding standards are based on the [PSR-1](https://www.php-fig.org/psr/psr-1/), [PSR-2](https://www.php-fig.org/psr/psr-2/) and [PSR-4](https://www.php-fig.org/psr/psr-4/) standards. This will be enfoced by the CI pipleine
* Code structure should conform with [Symfony's recommendations](https://symfony.com/doc/3.4/contributing/code/standards.html#structure)
* Symfony's [naming convention](https://symfony.com/doc/3.4/contributing/code/standards.html#naming-conventions) should be used
* Doc blocks should conform to [Symfony's recommendations](https://symfony.com/doc/3.4/contributing/code/standards.html#documentation)
### Convention (From SensioInsight)
* Projects must not depend on dependencies with known security issues
* Exceptions should not be enabled in production
* Logging should not be verbose in production
* Files should not be executable
* Public directory should not contain PHP files
* Missing use statement should be avoided
* The request service should never be injected
* The DocumentManager should not be flushed within a loop
* Global variable or function should never be used
* A GET action should not modify an existing resource
* Public methods in controller classes should only be actions (with the Action suffix)
* Symfony controller action method should not be too long (Slim controller, fat services)
* No absolute path should be hard-coded
* PHPUnit should be able to run all PHP tests
* Type hinting should be used whenever possible
* All PHP files should use strict typing
* Booleans and null should be compared strictly
* Code should not be duplicated
* The Symfony Dependency Injection Container should not be passed as an argument
* The Doctrine Entity Manager should not be passed as an argument
* Commented code should not be committed
* PHP code should not contain unreachable code, unused method, property, variable or parameter
* Cache or log files should not be committed
### Backward Compatibility policy
* Backward compatibility must be insured between minor versions
* Major version upgrade can make breaking changes
### Tests